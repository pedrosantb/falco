#include <QTRSensors.h>
                 
#define led 13

#define INB1 7     //define os pinos de controle do motor B
#define INB2 8
#define pwmb 9

#define INA1 5     //define os pinos de controle do motor A
#define INA2 6
#define pwma 3


int c=0, r=1;

int i =0; 

unsigned long lapStart=0, runningTime=0;

int erro = 0;

float valPid=0;
float kp=0.0278,kd=0.88;
int ultErro=0;

int velIn=180,velB=0,velA=0;
int velMax=255;

int in = 0;

QTRSensors qtr;

const uint8_t SensorCount = 6;           //numero de sensores utilizados
uint16_t sensorValues[SensorCount];
 

void calibrarQT()
{
   digitalWrite(led, HIGH);
 for (uint16_t i = 0; i < 400; i++)
  {
    qtr.calibrate();
  }
  digitalWrite(led, LOW);
  }


void setup() {
  pinMode(led,OUTPUT);

  pinMode(INA1,OUTPUT);
  pinMode(INA2,OUTPUT);
  pinMode(pwma,OUTPUT);

  pinMode(INB1,OUTPUT);
  pinMode(INB2,OUTPUT);
  pinMode(pwmb,OUTPUT); 
  
  qtr.setTypeAnalog();                    //Inicia os sensores como analogico 
  qtr.setSensorPins((const uint8_t[]){A0, A1, A2, A3, A4, A5}, SensorCount);

   delay(1000);
   

   calibrarQT();

}


void error()
{
  
  uint16_t posicao = qtr.readLineWhite(sensorValues);
  
  //Serial.println(posicao);  
  
  erro = 2500 - posicao; 
}

 void run(){
  
  
if(r==1){
  
  valPid = (kp*erro) + (kd*(erro-ultErro));
  ultErro = erro;
   

  velA = velIn - valPid;
  velB = velIn + valPid;
   
  if(velB>velMax) velB=velMax;
  if(velA>velMax) velA=velMax;

  if(velA<0) velA*=-1;
  if(velB<0) velB*=-1;

  
  if((valPid > 55)||(valPid < -55)){
    

   
  if(valPid > 0){
      digitalWrite(INA1,HIGH);
      digitalWrite(INA2,LOW);

      digitalWrite(INB1,LOW);
      digitalWrite(INB2,HIGH);
   }
   
  else if(valPid < 0){

      digitalWrite(INA1,LOW);
      digitalWrite(INA2,HIGH);

      digitalWrite(INB1,HIGH);
      digitalWrite(INB2,LOW);
    } 
  }

  else{
      
      
      digitalWrite(INA1,HIGH);
      digitalWrite(INA2,LOW);

      digitalWrite(INB1,HIGH);
      digitalWrite(INB2,LOW);
  }
  analogWrite(pwma, velB);
 analogWrite(pwmb, velA);
  
}
else stop();
}
 
 
 void stop(){
  
      digitalWrite(INA1,LOW);
      digitalWrite(INA2,LOW);

      digitalWrite(INB1,LOW);
      digitalWrite(INB2,LOW);
  }
  
void loop() {
       
        lapStart = millis();
        runningTime = millis();
        
        while((runningTime - lapStart)<15100){
              runningTime = millis();
              error();
              run();
     }
        
        r =0;
      
           
}
