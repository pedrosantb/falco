# Falco

EagleBotz's Falco code! &emsp; 2019/09 <br />

<p>---------------------------------------------------------------------------------------------------------------------------------------<br />
PT - BR<br />
Codigo para plataforma <b>Arduino Nano</b> de robô seguidor de linha basico utilizando<br />
algoritimo de controle PID simplificado.<br />
<p>---------------------------------------------------------------------------------------------------------------------------------------<br />
EN - UK<br />
Code for <b>Arduino Nano</b> platform of a basic line follower robot using a<br />
simplified PID control algorithm.<br />
<p>---------------------------------------------------------------------------------------------------------------------------------------<br />
<br />
<b>Hardware:</b><br />
 &emsp;      - Arduino Nano<br />
 &emsp;      - TB6612FNG<br />
 &emsp;      - Pololu IR sensor QTR-8RC<br />
 &emsp;      - Pololu's N20 motors<br />  
  
   
Made by: <b>Pedro StªBárbarar</b>
        